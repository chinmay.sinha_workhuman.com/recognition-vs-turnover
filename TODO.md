* Establish what date range to read awards for. e.g., if we are assessing the turonver for calendar year 2022, should we 
  * Use awards for the calendar year 2022?
  * Use awards for the calendar year 2021?
  * Conditionally establish what awards date range to use? e.g. default to 2022 calendar year, but if someone left in mid-2022 calendar year, then use this as the end date & 1 year prior to that as the start date?
  
